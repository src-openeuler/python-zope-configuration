%global _empty_manifest_terminate_build 0
Name:		python-zope-configuration
Version:	5.0.1
Release:	1
Summary:	Zope Configuration Markup Language (ZCML)
License:	ZPL-2.1
URL:		https://github.com/zopefoundation/zope.configuration
Source0:	https://github.com/zopefoundation/zope.configuration/archive/%{version}/zope.configuration-%{version}.tar.gz
BuildArch:	noarch

%description
The Zope configuration system provides an extensible system for supporting various kinds of configurations.
It is based on the idea of configuration directives. Users of the
configuration system provide configuration directives in some
language that express configuration choices. The intent is that the
language be pluggable. An XML language is provided by default.

%package -n python3-zope-configuration
Summary:	Zope Configuration Markup Language (ZCML)
Provides:	python-zope-configuration
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:       python3-zope.i18nmessageid
Requires:       python3-zope-schema

%description -n python3-zope-configuration
The Zope configuration system provides an extensible system for supporting various kinds of configurations.
It is based on the idea of configuration directives. Users of the
configuration system provide configuration directives in some
language that express configuration choices. The intent is that the
language be pluggable. An XML language is provided by default.

%package help
Summary:	Development documents and examples for zope.configuration
Provides:	python3-zope-configuration-doc

%description help
Development documents and examples for zope.configuration.

%prep
%autosetup -n zope.configuration-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-zope-configuration -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_pkgdocdir}

%changelog
* Thu Aug 08 2024 yuanlipeng <yuanlipeng2@huawei.com> - 5.0.1-1
- Update to 5.0.1

* Sat May 06 2023 wubijie <wubijie@kylinos.cn> - 5.0-1
- Update package to version 5.0

* Wed Sep 14 2022 Qiao Jijun <qiaojijun@kylinos.cn> - 4.4.1-1
- Update to 4.4.1

* Thu Dec 17 2020 Python_Bot <Python_Bot@openeuler.org> - 4.4.0-1
- Package Spec generated
